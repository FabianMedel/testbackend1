# TestBackend1

> Install with Docker

- docker-compose build 
- docker-compose up

> Install 
- npm install (install modules)

> Run 
- node app.js

## Endpoints 

> [GET] localhost:3000/authorizationQuery

> [GET] localhost:3000/authorizationBearer

