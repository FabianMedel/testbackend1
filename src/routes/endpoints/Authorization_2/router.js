const router = require('express').Router();
var  axios = require('axios');

router.route('/').get(async (req,res) =>{
    try {
        const { authorization } = req.headers;
        const [authType, token] = authorization.trim().split(' ');
        
        if (authType !== 'Bearer') throw new Error('Expected a Bearer token');

        axios.post(
            'https://atlantia-dev-test.herokuapp.com/api/profile',
            { 
                headers: {
                    'authorization': `Bearer ${token}`                
                }
            }
        )
        .then((response) => {
            res.status(response.status).send(response.data);
        })
        .catch((error) => {
            res.status(error.response.status).send(error.response.data);
        });
    } catch (error) {
        res.status(500).send({
            message: 'Hubo un error',
            error: error.message
        });
    }
});

module.exports = router;