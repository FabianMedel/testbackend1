const router = require('express').Router();
var  axios = require('axios');

router.route('/').get(async (req,res) =>{
    try {
        const { authuser } = req.query; 
        axios.get(
            'https://atlantia-dev-test.herokuapp.com/api/auth',
            { 
                params: {
                    authuser: `${authuser}`
                }
            })
            .then((response) => {
                res.status(response.status).send(response.data);
            })
            .catch((error) => {
                res.status(error.response.status).send(error.response.data);
            });
    } catch (error) {
        res.status(500).send({
            message: 'Hubo un error',
            error: error.message
        });
    }
});

module.exports = router;