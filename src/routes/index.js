const router = require('express').Router();

router.use('/authorizationQuery', require('./endpoints/Authorization_1/router'));
router.use('/authorizationBearer', require('./endpoints/Authorization_2/router'));


module.exports = router;